import xml.etree.ElementTree as ET

operators = {}

def make_print(params):
    return {
        'title': 'PRINT',
        'color': '#f6cbbd',
        'inputs': ['in'],
        'outputs': []
    }
operators['print'] = make_print

def make_serial_reader(params):
    return  {
        'title': 'SERIAL<br/>READER',
        'color': '#acdbd2',
        'inputs': [],
        'outputs': ['out']
    }
operators['serial_reader'] = make_serial_reader

def make_serial_writer(params):
    return {
        'title': 'SERIAL<br/>WRITER',
        'color': '#acdbd2',
        'inputs': ['in'],
        'outputs': []
    }
operators['serial_writer'] = make_serial_writer

def make_pipe_reader(params):
    return  {
        'title': 'PIPE<br/>READER',
        'color': '#b6d8f2',
        'inputs': [],
        'outputs': ['out']
    }
operators['pipe_reader'] = make_pipe_reader

def make_pipe_writer(params):
    return {
        'title': 'PIPE<br/>WRITER',
        'color': '#b6d8f2',
        'inputs': ['in'],
        'outputs': []
    }
operators['pipe_writer'] = make_pipe_writer

def make_reliable_writer(params):
    return {
        'title': 'RELIABLE<br/>WRITER',
        'color': '#f6cbbd',
        'inputs': ['in', 'ack'],
        'outputs': ['out']
    }

operators['reliable_writer'] = make_reliable_writer

def make_reliable_reader(params):
    return {
        'title': 'RELIABLE<br/>READER',
        'color': '#f6cbbd',
        'inputs': ['in'],
        'outputs': ['out', 'ack']
    }
operators['reliable_reader'] = make_reliable_reader

def make_split(params):
    return {
        'title': 'SPLIT',
        'color': '#fff59d',
        'inputs': ['in'],
        'outputs': [f'out_{i}' for i in range(int(params['n']))]
    }
operators['split'] = make_split

def make_merge(params):
    return {
        'title': 'MERGE',
        'color': '#fff59d',
        'inputs': [f'in_{i}' for i in range(int(params['n']))],
        'outputs': ['out']
    }
operators['merge'] = make_merge

f = open('scheme.dot', 'w')
f.write('digraph G {\n')
f.write('rankdir=LR;\n')

tree = ET.parse('example.xml')
root = tree.getroot()


for elem in root.findall('operator'):
    name = elem.attrib['name']
    parameters = {}
    for parameter in elem.findall('parameter'):
        parameters[parameter.attrib['key']] = parameter.attrib['value']

    d = operators[elem.attrib['type']](parameters)
    print(d)

    f.write(f'''{name} [shape=none, label=<''')
    f.write('''<table BORDER='0' CELLBORDER='0' CELLSPACING='0' CELLPADDING='0'>''')
    f.write(f'''<tr><td bgcolor='{d['color']}'  BORDER='1' CELLPADDING='1' COLSPAN='2'><font point-size="12"><b>{d['title']}</b><br/></font><font point-size="10">{name}</font></td></tr><tr>''')

    only_inputs = len(d['inputs']) > 0 and len(d['outputs']) == 0
    only_outputs = len(d['outputs']) > 0 and len(d['inputs']) == 0

    if len(d['inputs']) > 0:
        f.write(f'''<td{" COLSPAN='2'" if only_inputs else ""}>''')
        f.write('''<table BORDER='0' CELLBORDER='1' CELLSPACING='0' CELLPADDING='4'>''')

        for input in d['inputs']:
            f.write(f'''<tr><td PORT='in_{input}' BORDER='1' ALIGN="LEFT" ><font point-size="12">{input}</font></td></tr>''')

        f.write('''</table>''')
        f.write('''</td>''')
        pass

    if len(d['outputs']) > 0:
        f.write(f'''<td{" COLSPAN='2'" if only_outputs else ""}>''')
        f.write('''<table BORDER='0' CELLBORDER='1' CELLSPACING='0' CELLPADDING='4'>''')

        for output in d['outputs']:
            f.write(f'''<tr><td PORT='out_{output}' BORDER='1' ALIGN="RIGHT" ><font point-size="12">{output}</font></td></tr>''')

        f.write('''</table>''')
        f.write('''</td>''')
        pass

    f.write('''<td COLSPAN='2'></td>''')
    f.write('''</tr></table>>];\n''')

for elem in root.findall('connect'):
    producer, producer_port = elem.attrib['producer'].split('.')
    consumer, consumer_port = elem.attrib['consumer'].split('.')

    f.write(f'''{producer}:out_{producer_port}:e -> {consumer}:in_{consumer_port}:w\n''')

    pass

f.write('}\n')
